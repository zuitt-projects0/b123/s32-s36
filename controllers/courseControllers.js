const Course = require('../models/Course')

module.exports.addCourse = (req,res) => {
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	})
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err))
}

module.exports.getAllCourses = (req,res) => {
	Course.find()
	.then(foundCourses =>res.send(foundCourses))
	.catch(error => res.send(error))
}

module.exports.getActiveCourses = (req,res) => {
	Course.find({isActive:true})
	.then(activeCourses => res.send(activeCourses))
	.catch(error => res.send(error))
}

module.exports.getSingleCourse = (req,res) => {
	Course.findById(req.params.id)
	.then(foundCourse => res.send(foundCourse))
	.catch(error => res.send(error))
}

module.exports.updateCourse = (req,res) => {
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Course.findByIdAndUpdate(req.params.id,updatedCourse,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error))
}

module.exports.archiveCourse = (req,res) => {
	let updatedCourse = {
		isActive: false
	}
	Course.findByIdAndUpdate(req.params.id,updatedCourse,{new:true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(error => res.send(error))
}

module.exports.activateCourse = (req,res) => {
	let updatedCourse = {
		isActive: true
	}
	Course.findByIdAndUpdate(req.params.id,updatedCourse,{new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(error => res.send(error))
}

module.exports.getEnrollees = (req,res) => {
	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))
	.catch(err => res.send(err))
}

module.exports.checkDuplicate = (req,res) => {
	Course.findOne({name:req.body.name})
	.then(result => {
		if(result !== null){
			return res.send({
				isRegisteredCourse: true,
				message:"Duplicate Course Found."
			})
		}
		else{
			return res.send({
				isRegisteredCourse: false,
				message:"This course is not in our database."
			})
		}
	})
	.catch(err => res.send(err))
}

module.exports.getInactive = (req,res) => {
	Course.find({isActive:false})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}