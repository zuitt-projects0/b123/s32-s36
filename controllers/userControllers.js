const User = require('../models/User');
const Course = require("../models/Course");
const bcrypt = require('bcrypt');
const auth = require('../auth');
const {createAccessToken} = auth;

module.exports.registerUser = (req,res) => {
	console.log(req.body);
	if(req.body.password.length < 8) return res.send({message: "Password is too short."})
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW,
		isAdmin: req.body.isAdmin
	})
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
}

module.exports.loginUser = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({message: "No User Found."})
		} else {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
		console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({accessToken: createAccessToken(result)});
			} else {
				return res.send({message: "Password is incorrect."})
			}
		}
	})
	.catch(err => res.send(err))
}

module.exports.getSingleUser = (req,res) => {
	console.log(req.user);
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateProfile = (req,res) => {
	console.log(req.user);
	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}
	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}


module.exports.enroll = async (req,res) => {
	if(req.user.isAdmin) return res.send({
		auth: "Failed",
		message: "Action Forbidden"
	});

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		user.enrollments.push({courseId: req.body.courseId})
		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	console.log(isUserUpdated);
	if(isUserUpdated !== true) return res.send(isUserUpdated);
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		course.enrollees.push({userId: req.user.id})
		return course.save()
		.then(user => true)
		.catch(err => err.message)
	})
	console.log(isCourseUpdated);
	if(isCourseUpdated !== true) return res.send(isCourseUpdated);
	if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Successfully.");
}

module.exports.checkEmailExists = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({
				isAvailable: true,
				message: "Email Available."
			})
		} else {
			return res.send({
				isAvailable: false,
				message: "Email is already registered."
			})
		}
	})
	.catch(err => res.send(err));

}

module.exports.getEnrollments = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err))
}

module.exports.getNonAdminUser = (req,res) => {
	User.find({isAdmin: false})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateNonToAdmin = (req,res) => {
	let updatesAd = {
		isAdmin: req.body.isAdmin
	}
	User.findByIdAndUpdate(req.params.id,updatesAd,{new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}