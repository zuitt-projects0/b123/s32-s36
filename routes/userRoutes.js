const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const {
	registerUser,
	loginUser,
	getSingleUser,
	updateProfile,
	enroll,
	checkEmailExists,
	getEnrollments,
	getNonAdminUser,
	updateNonToAdmin
} = userControllers

const auth = require('../auth')
const {verify,verifyAdmin} =  auth;

router.post('/',registerUser);

router.post('/login',loginUser);

router.get('/getUserDetails',verify,getSingleUser);

router.put('/updateProfile',verify,updateProfile);

router.post('/enroll',verify,enroll);

router.post('/checkEmailExists',checkEmailExists)

router.get('/getEnrollments',verify,getEnrollments)

router.get('/getNonAdminUser',verify,verifyAdmin,getNonAdminUser)

router.put('/updateNonToAdmin/:id',verify,verifyAdmin,updateNonToAdmin)

module.exports = router;