const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const app = express()
const port = process.env.PORT || 4000

mongoose.connect("mongodb+srv://User1:lokomoko12@cluster0.hy0hl.mongodb.net/bookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let db = mongoose.connection
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open",()=>console.log("Connected To MongoDB"))

app.use(express.json())

const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes)

const courseRoutes = require('./routes/courseRoutes')
app.use('/courses',courseRoutes)

app.listen(port, ()=>console.log(`Server running at port ${port}`))
